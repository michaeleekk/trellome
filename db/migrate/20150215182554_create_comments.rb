class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :comment_id
      t.string :user_id
      t.boolean :is_read

      t.timestamps null: false
    end
  end
end
