class CreateTrellos < ActiveRecord::Migration
  def change
    create_table :trellos do |t|
      t.string :session_id
      t.integer :user_id
      t.string :trello_id
      t.string :access_token

      t.timestamps null: false
    end
  end
end
