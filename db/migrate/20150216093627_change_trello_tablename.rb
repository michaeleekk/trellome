class ChangeTrelloTablename < ActiveRecord::Migration
  def change
    rename_table :trellos, :trello_auths
    rename_column :comments, :trello_id, :trello_auth_id
  end
end
