class AddCommentTrelloId < ActiveRecord::Migration
  def change
    add_column :comments, :trello_id, :integer
  end
end
