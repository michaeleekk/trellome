// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var Avatar = React.createClass({
  render: function() {
    var member = this.props.member;
    if (member.avatarHash === null) {
      return (
        <div className="avatar-container">
          <span>{member.initials}</span><br />
        </div>
      );
    } else {
      return (
        <div className="avatar-container">
          <img src={member.avatarUrl} /><br />
        </div>
      );
    }
  }
});

var CommentForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var reply = this.refs.reply.getDOMNode().value.trim();
    var card_id = this.refs.card_id.getDOMNode().value.trim();
    var token = this.refs.token.getDOMNode().value.trim();
    if (!reply) {
      alert('Please input a reply.');
      return;
    } else if ( !card_id || !token) {
      return;
    }
    this.props.onReplySubmit({reply: reply, card_id: card_id, token: token});
    this.refs.reply.getDOMNode().value = '';
  },
  render: function() {
    return (
      <form id="add_comment_form" className="commentForm" onSubmit={this.handleSubmit}>
        <input type="hidden" name="authenticity_token" ref="token" value={this.props.token} />
        <input type="hidden" name="card_id" ref="card_id" value={this.props.card_id} />
        <textarea name="reply" ref="reply" defaultValue={this.props.template_reply} />
        <button type="submit" name="commit" value="Reply" className="btn btn-flat btn-default send-btn mdi-content-send" />
      </form>
    );
  }
});

var Comment = React.createClass({
  handleReplySubmit: function(comment) {
    $('.isreplied-btn',this.getDOMNode()).addClass('isreplied');
    $.ajax({
      url: this.props.reply_url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        console.log('isreplied success');
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleIsreadSubmit: function(e) {
    e.preventDefault();

    $('.isread-btn',this.getDOMNode()).toggleClass('isread');

    var comment_id = this.refs.comment_id.getDOMNode().value.trim();
    var value = this.refs.value.getDOMNode().value.trim();
    var token = this.refs.token.getDOMNode().value.trim();
    if (!token || !value || !comment_id) {
      return;
    }
    this.refs.comment_id.getDOMNode().value = '';
    this.refs.value.getDOMNode().value = '';
    this.refs.token.getDOMNode().value = '';

    var data = {comment_id: comment_id, value: value, authenticity_token: token};
    $.ajax({
      url: this.props.isread_url,
      dataType: 'json',
      type: 'POST',
      data: data,
      success: function(data) {
        console.log('isread success');
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.isread_url, status, err.toString());
      }.bind(this)
    });
  },
  render: function() {
    var me_trello = this.props.me_trello;
    var comment = this.props.comment;
    var token = this.props.token;
    var rawMarkup = this.props.children.toString();
    return (
        <div className="comment-card">
          <div className="card-left">
        <Avatar member={comment.creator} />
      </div>
          <div className="card-right">
              <span>
            <b>{comment.creator.username}</b> - <span title={comment.date} className="time">{comment.format_date} ago</span>
          </span>
          <br />
              <span>
            <a href={'http://trello.com/b/' + comment.board.shortLink}>{comment.board.name}</a> / <a href={'http://trello.com/c/' + comment.card.shortLink}>{comment.card.name}</a>
          </span>
          <br />
              <div className="comment-content" dangerouslySetInnerHTML={{__html: rawMarkup}}></div>
    
          <CommentForm card_id={comment.card.id} comment={comment} template_reply={'@' + comment.creator.username + ' '} me_trello={me_trello} onReplySubmit={this.handleReplySubmit} token={token} />

          <form className="set_isread_form" onSubmit={this.handleIsreadSubmit}>
            <input type="hidden" name="authenticity_token" ref="token" value={token} />
            <input type="hidden" name="comment_id" ref="comment_id" value={comment.id} />
            <input type="hidden" name="value" ref="value" value={comment.isread} />
            <button className={'btn btn-flat btn-default isread-btn mdi-action-done ' + (comment.isread ? 'isread': '')} />
          </form>
          <button className={'btn btn-flat btn-default isreplied-btn mdi-action-done ' + (comment.isreplied ? 'isreplied': '')} />
          </div>
        </div>
    );
  }
});
var CommentBox = React.createClass({
  handleCommentSubmit: function(comment) {
    var comments = this.state.data;
    var newComments = comments.concat([comment]);
    this.setState({data: newComments});
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  render: function() {
    return (
      <div className="commentBox">
        <CommentBox author={comment.author}>
        {comment.text}
        </CommentBox>
        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div>
    );
  }
});

var CommentList = React.createClass({
  getInitialState: function() {
    return null;
  },
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      success: function(data) {
        this.setState(data);
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    // init state
    if (!this.state) {
      return (
        <div className="comment-list">
          <span className="loading glyphicon glyphicon-refresh glyphicon-refresh-animate" />
        </div>
      );
    }
    
    if (this.state.comments.length) {
      var me_trello = this.state.me_trello;
      var token = this.props.token;
      var commentNodes = this.state.comments.map(function (comment) {
        return (
          <Comment comment={comment} me_trello={me_trello} reply_url="/add_comment" isread_url="/set_isread" token={token}>
          {comment.text}
          </Comment>
        );
      });
      return (
        <div className="comment-list">
          {commentNodes}
        </div>
      );
    } else {
      return (
        <div className="comment-list">
          <div>There is no comments mentioned about you.</div>
        </div>
      );
    }
  }
});

