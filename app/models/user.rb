class User < ActiveRecord::Base
  has_many :comments
  has_one :trello_auth

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :omniauthable, :recoverable and :trackable 
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable
end
