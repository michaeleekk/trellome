include TextHelper

class Comment < ActiveRecord::Base
  belongs_to :user

  validates :trello_auth_id, presence: true
  validates :comment_id, presence: true


  # self.primary_keys = :trello_auth_id, :comment_id

  def self.get_isread mentioned_comments, trello_auth, current_username
    mentioned_comments.each do |comment|
      # add the isread field
      this_comment = self.where(trello_auth_id: trello_auth.id, comment_id: comment['id']).take
      if this_comment != nil
        comment['isread'] = true
      else
        comment['isread'] = false
      end
      comment['isreplied'] = false

      comment['data']['text'] = TextHelper.all comment['data']['text'], current_username
    end

    mentioned_comments
  end
end
