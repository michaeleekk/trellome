class TrelloAuth < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  validates :trello_id, presence: true
end
