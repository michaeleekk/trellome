json.array!(@trello_auths) do |trello_auth|
  json.extract! trello_auth, :id, :session_id, :user_id, :trello_id, :access_token
  json.url trello_auth_url(trello_auth, format: :json)
end
