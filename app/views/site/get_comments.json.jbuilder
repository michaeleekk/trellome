json.me_trello do
  json.avatarHash @me_trello['avatarHash']
  json.avatarUrl @me_trello['avatarUrl']
  json.initials @me_trello['initials']
end

json.comments @trello_comments do |comment|
  json.isread comment['isread']
  json.isreplied comment['isreplied']
  json.id comment['id']
  json.date comment['date']
  json.format_date comment['formatDate']
  json.text comment['data']['text']
  json.creator comment['memberCreator']
  json.board comment['data']['board']
  json.card comment['data']['card']
end
