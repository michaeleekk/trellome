json.array!(@comments) do |comment|
  json.extract! comment, :id, :id, :user_id, :comment_id, :is_read
  json.url comment_url(comment, format: :json)
end
