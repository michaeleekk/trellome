class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    #@comments = Comment.all
    #Rails.logger.debug("current_user: #{current_user.inspect}\n")
    # @comments = current_user.trello_auth.comments
    @comments = current_user.trello_auth.comments
  end

  # GET /comments.all
  def all
    @comments = Comment.all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def set_isread
    puts 'set_isread'
    puts params
    puts
    value = params[:value] != 'true'
    comment_id = params[:comment_id]

    # check if trello+comment pair exists
    comment_exists = Comment.exists?(trello_auth_id: current_user.trello_auth.id, comment_id: comment_id)

    puts value, comment_id, comment_exists
    puts 'a0'
    if value and not comment_exists
      puts 'a'
      @comment = Comment.new(trello_auth_id: current_user.trello_auth.id, comment_id: comment_id)
      if @comment.save
        puts 'b'
        success = true
      end
    elsif not value and comment_exists
      puts 'c'
      @comment = Comment.where(trello_auth_id: current_user.trello_auth.id, comment_id: comment_id).take
      @comment.destroy
      success = true
    end

    respond_to do |format|
      if value
        if success
          format.html { redirect_to '/', notice: 'Comment marked as read.' }
          format.json { head :no_content }
        else
          format.html { redirect_to '/', notice: 'Unable to mark as read.' }
          format.json { render json: { :message => 'Unable to mark as read.' }, status: 400 }
        end
      else
        format.html { redirect_to '/', notice: 'Comment marked as unread.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:id, :is_read, :comment_id, :trello_auth_id, :user_id)
    end
end
