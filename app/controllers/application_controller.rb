class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # def configure_devise_params
  #   devise_parameter_sanitizer.for(:edit) do |u|
  #    u.permit(:user_id, :session_id, :access_token)
  #   end
  # end

  # before_filter :configure_permitted_parameters


  # def configure_permitted_parameters
  #   devise_parameter_sanitizer.for(:edit).permit(:user_id, :session_id, :access_token)
  # end
end
