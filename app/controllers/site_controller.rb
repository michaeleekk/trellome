require 'trello_lib'

class SiteController < ApplicationController
  before_action :authenticate_user!
  before_action :set_trello_auth, only: [:index, :add_comment, :get_comments]

  include TextHelper

  def set_trello_auth
    if not TrelloAuth.exists?(user_id: current_user.id)
      redirect_to trello_oauth_path
      return
    end

    token = current_user.trello_auth.access_token
    @t = TrelloLib.new token
  end

  def get_comments
    @me_trello = @t.get_user 'me'
    @trello_username = @me_trello['username']
    @me_avatar_url = @me_trello['avatarUrl']
    @trello_comments = Comment.get_isread @t.get_mentioned_comments, current_user.trello_auth, @trello_username

    avatar_urls = {}
    @trello_comments.each do |comment|
      # readable timestamp
      comment['formatDate'] = TextHelper.format_relative_time(comment['date'])

      # add avatar to the creator
      creator_id = comment['memberCreator']['id']
      if avatar_urls.has_key?(creator_id)
        comment['memberCreator']['avatarUrl'] = avatar_urls[creator_id]
      else
        avatar_url = @t.get_avatar_url creator_id, :size => :small
        comment['memberCreator']['avatarUrl'] = avatar_url
      end
    end
  end

  def index
    @me_trello = @t.get_user 'me'
  end

  def add_comment
    @ret = @t.add_comment params['card_id'], params['reply']
    respond_to do |format|
      if @ret
        format.html { redirect_to '/', notice: 'Replied.' }
        format.json { redirect_to '/get_comments' }
      else
        format.html { redirect_to '/', notice: 'Failed to reply the comment.' }
        format.json { render json: { :message => 'error' }, status: :unprocessable_entity }
      end
    end
  end

  def oauth_redirect
      redirect_to TrelloLib.step1_authorize_url
  end

  def trello_cb
    render layout: false
  end

  def test
    @trello_auth = current_user.trello_auth
    respond_to do |format|
      format.html { render :test, notice: 'Clicked test' }
      # format.json { render json: { :message => 'error' }, status: :unprocessable_entity }
      # format.json { render json: { :message => 'error' }, status: 400 }
      format.json { head :no_content }
    end
  end

  def test_reply
    @aa = params['hehe']
    @bb = params['commit']
  end
end
