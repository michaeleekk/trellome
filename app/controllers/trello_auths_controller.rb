class TrelloAuthsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_trello_auth, only: [:show, :edit, :update, :destroy]

  # GET /trello_auths
  # GET /trello_auths.json
  def index
    #@trellos = Trello.all
    #@trellos = TrelloLib.new token
    @trello_auths = TrelloAuth.all
  end

  # GET /trello_auths/1
  # GET /trello_auths/1.json
  def show
  end

  # GET /trello_auths/new
  def new
    @trello_auth = TrelloAuth.new
  end

  # GET /trello_auths/1/edit
  def edit
  end

  # POST /trello_auths
  # POST /trello_auths.json
  def create
    @trello_auth = TrelloAuth.new(trello_params)

    respond_to do |format|
      if @trello_auth.save
        format.html { redirect_to @trello_auth, notice: 'Trello was successfully created.' }
        format.json { render :show, status: :created, location: @trello_auth }
      else
        format.html { render :new }
        format.json { render json: @trello_auth.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trello_auths/1
  # PATCH/PUT /trello_auths/1.json
  def update
    respond_to do |format|
      if @trello_auth.update(trello_auth_params)
        format.html { redirect_to @trello_auth, notice: 'Trello was successfully updated.' }
        format.json { render :show, status: :ok, location: @trello_auth }
      else
        format.html { render :edit }
        format.json { render json: @trello_auth.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_oauth
    # validate
    begin
      token = params['token']
      t = TrelloLib.new token
      me_trello = t.get_user 'me'
      trello_id = me_trello['id']
    #rescue Trello::Error
    rescue
      redirect_to '/', notice: 'Some problems occurred when updating oauth. 1'
      return
    end

    puts "update_oauth: #{params} #{token}"

    @trello_auth = current_user.trello_auth
    if @trello_auth
      @trello_auth.update(user_id: 0, access_token: '')
      if not @trello_auth.save
        redirect_to '/', notice: 'Some problems occurred when updating oauth. 2'
        return
      end
    end

    @trello_auth_token = TrelloAuth.where(trello_id: trello_id).take
    if @trello_auth_token
      if @trello_auth_token.user_id == 0
        @trello_auth_token.user_id = current_user.id
        @trello_auth_token.access_token = token
      else
        redirect_to '/', notice: 'Problem occurs when linking the Trello account.'
        return
      end
    else
      @trello_auth_token = TrelloAuth.new(user_id: current_user.id, trello_id: trello_id, access_token: token)
    end
    if not @trello_auth_token.save
      redirect_to '/', notice: 'Some problems occurred when updating oauth. 3'
      return
    end

    redirect_to '/', notice: 'TrelloAuth was successfully updated. 4'
    return
  end

  # DELETE /trello_auths/1
  # DELETE /trello_auths/1.json
  def destroy
    @trello_auth.destroy
    respond_to do |format|
      format.html { redirect_to trellos_url, notice: 'Trello was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trello_auth
      @trello_auth = TrelloAuth.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trello_auth_params
      params.require(:trello_auth).permit(:session_id, :user_id, :trello_id, :access_token)
    end
end
