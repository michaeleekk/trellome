require 'kramdown'

include ActionView::Helpers::AssetTagHelper
include ActionView::Helpers::SanitizeHelper
include ActionView::Helpers::DateHelper
include ActionView::Helpers::AssetTagHelper
include ERB::Util

module TextHelper
  def all(content, current_username)
    comment_text = kramdown newline content
    comment_text = highlight_username comment_text, current_username
    comment_text = emojify comment_text
    comment_text = trello_hyperlink comment_text
  end

  def trello_hyperlink(content)
    h(content).to_str.gsub(/https:\/\/trello.com\/(.)\/([\w+-]+)\/([\w+-]+)/) do |match|
      if $1 == 'c'
        type = 'Card'
      elsif $1 == 'b'
        type = 'Board'
      end

      %(<a href="#{match}">#{type} \##$3</a> )
    end.html_safe if content.present?
  end


  def highlight_username(content, current_username)
    h(content).to_str.gsub(/@([\w+-]+) /) do |match|
      if $1 == current_username
        %(<span class="username-highlight-self">@#$1</span> )
      else
        %(<span class="username-highlight-others">@#$1</span> )
      end
    end.html_safe if content.present?
  end

  def newline(content)
    tmp = h(content).to_str.gsub(/[\n]+/) do |match|
      %(\n)
    end
    tmp = tmp.gsub(/([^>])[\n]+/) do |match|
      %(#$1<br />)
    end
    tmp = tmp.gsub(/([a-zA-Z]?)>[\n]+/) do |match|
      if not ['li', 'ol', 'ul', 'p'].include?($1)
        %(#$1><br />)
      else
        match
      end
    end.html_safe if content.present?
  end

  def format_relative_time(content)
    time_ago_in_words content.to_time
  end

  def format_time(content)
    content.to_time.to_formatted_s :db
  end

  def kramdown(content)
    sanitize Kramdown::Document.new(content, :input => 'markdown').to_html
  end

  def emojify(content)
    h(content).to_str.gsub(/:([\w+-]+):/) do |match|
      if emoji = Emoji.find_by_alias($1)
        %(<img alt="#$1" src="#{image_path("emoji/#{emoji.image_filename}")}" style="vertical-align:middle" width="20" height="20" />)
      else
       match
      end
    end.html_safe if content.present?
  end
end
