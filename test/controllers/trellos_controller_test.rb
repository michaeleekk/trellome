require 'test_helper'

class TrellosControllerTest < ActionController::TestCase
  setup do
    @trello = trellos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:trellos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create trello" do
    assert_difference('Trello.count') do
      post :create, trello: { access_token: @trello.access_token, session_id: @trello.session_id, trello_id: @trello.trello_id, user_id: @trello.user_id }
    end

    assert_redirected_to trello_path(assigns(:trello))
  end

  test "should show trello" do
    get :show, id: @trello
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @trello
    assert_response :success
  end

  test "should update trello" do
    patch :update, id: @trello, trello: { access_token: @trello.access_token, session_id: @trello.session_id, trello_id: @trello.trello_id, user_id: @trello.user_id }
    assert_redirected_to trello_path(assigns(:trello))
  end

  test "should destroy trello" do
    assert_difference('Trello.count', -1) do
      delete :destroy, id: @trello
    end

    assert_redirected_to trellos_path
  end
end
