Rails.application.routes.draw do
  # get 'site/index'
  devise_for 'user'

  post 'add_comment' => 'site#add_comment'
  post 'set_isread' => 'comments#set_isread'
  get 'trello_oauth' => 'site#oauth_redirect'
  get 'trello_cb' => 'site#trello_cb'
  get 'trello_oauth_back' => 'trello_auths#update_oauth'
  get 'test' => 'site#test'
  get 'get_comments' => 'site#get_comments'
  root 'site#index'

  # :TODO: remove when production
  get 'all' => 'comments#all'
  get 'comments/all' => 'comments#all'
  resources :comments
  resources :trello_auths
end
