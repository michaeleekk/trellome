$LOAD_PATH.unshift 'lib'
require 'timeout'
require 'json'
require 'trello'

include Trello

def repeat_timeout times: 3, timeout: 5
  times.times do
    begin
      ret = Timeout::timeout(timeout) {
        yield
      }
    rescue Timeout::Error
    end
    if ret
      return ret
    end
  end

  raise Timeout::Error
end

class TrelloLib

  def initialize access_token, trello_key: nil, trello_secret: nil
    if trello_key then
      @client = Trello::Client.new(
        :consumer_key => trello_key,
        :consumer_secret => trello_secret,
        :oauth_token => access_token
      )
    else
      @client = Trello::Client.new(
        :consumer_key => Rails::application.config.trello_key,
        :consumer_secret => Rails::application.config.trello_secret,
        :oauth_token => access_token
      )
    end
  end

  def get_avatar_url user_id, size: :large
    return repeat_timeout {
      @client.find(:members, user_id).avatar_url :size => size
    }
  end

  def get_user user_id
    return repeat_timeout {
      user = JSON.parse(@client.get("/members/#{user_id}"))
      if user['avatarHash']
        user['avatarUrl'] = self.get_avatar_url user_id, :size => :small
      end
      user
    }
  end

  def get_username
    return repeat_timeout {
      @client.find(:members, 'me').username
    }
  end

  def get_mentioned_comments
    mentionedOnCards = repeat_timeout {
      @client.get("/members/me/notifications", {
        :filter => "mentionedOnCard"
      })
    }

    notifications = JSON.parse(mentionedOnCards)

    for n in notifications
      if n["type"] != "mentionedOnCard" then
        data = n["data"]
        print "N [#{n["memberCreator"]["username"]}] [#{n["memberCreator"]["full_name"]}] #{n["id"]}: #{n["type"]}\n"
      else
        data = n["data"]
        print "N [#{n["memberCreator"]["username"]}]"
        print " [#{n["memberCreator"]["id"]}]"
        print " [#{data["board"]["name"]}]"
        print " [#{data["card"]["name"]}] #{n["id"]}: #{data["text"]}\n"
      end
    end

    return notifications
  end

  def add_comment card_id, text
    # check length of text within 1 to 16384
    repeat_timeout {
      @client.post("/cards/#{card_id}/actions/comments", {
        :text => text
      })
    }
  end

  def self.step1_authorize_url
    trello_key = Rails.application.config.trello_key
    trello_app_name = Rails.application.config.trello_app_name
    trello_redirect_uri = Rails.application.config.trello_redirect_uri
    return "https://trello.com/1/authorize?key=#{trello_key}&name=#{trello_app_name}&response_type=token&scope=read,write,account&expiration=never&callback_method=fragment&return_url=#{trello_redirect_uri}"
  end
end
